My first project for learning Bootstrap 4.

Simple landing page for non - existent conference.
BS enabled the fast development.

Project includes:
- grid and its utilities
- fully responsive website
- displaying content with cards
- schedule built with list groups
- pop - up using modal windows for registration
- structured form layouts with grid
- different menus
- informative content covering Marvel Universe :)

Effect of workshops